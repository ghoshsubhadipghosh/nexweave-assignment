import { FaRocket } from "react-icons/fa";
import { BsGrid1X2Fill, BsFillPlusCircleFill } from "react-icons/bs";
import { HiSpeakerphone } from "react-icons/hi";
import { SiWebmoney } from "react-icons/si";
import { AiFillSetting } from "react-icons/ai";
import { Campaign } from "../pages/campaigns/Campaign";
import { DefaultPage } from "../pages/default";

function useAppRoutes() {
  const getSidebarMenuItems = () => {
    return [
      {
        name: "Get Started",
        Icon: FaRocket,
        url: "/get-started",
        component: DefaultPage,
      },
      {
        name: "Templates",
        Icon: BsGrid1X2Fill,
        url: "/templates",
        component: DefaultPage,
      },
      {
        name: "Campaigns",
        Icon: HiSpeakerphone,
        url: "/",
        component: Campaign,
      },
      {
        name: "Integrations",
        Icon: BsFillPlusCircleFill,
        url: "/integrations",
        component: DefaultPage,
      },
      {
        name: "Manage Websites",
        Icon: SiWebmoney,
        url: "/manage-websites",
        component: DefaultPage,
      },
      {
        name: "Settings",
        Icon: AiFillSetting,
        url: "/settings",
        component: DefaultPage,
      },
    ];
  };
  return {
    getSidebarMenuItems,
  };
}

export { useAppRoutes };
