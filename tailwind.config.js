module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: {
          DEFAULT: "#4D148C",
          dark: "#321B4C",
        },
        secondary: "#FF7A59",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
