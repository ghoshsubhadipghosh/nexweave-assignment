import React from 'react'
import { H1 } from '../../components/utils/H1'
import { CreateCampaignButton } from './CreateCampaignButton'
import { RefreshIcon } from './RefreshIcon'

export const Header = () => {
  return (
    <div className="px-2 py-2 outline-black flex justify-between items-center">
      {/* Right Hand Side */}
      <H1 classes="px-3 w-4/5">
        Campaign
      </H1>
      {/* Left Hand Side */}
      <div className="flex items-center justify-around w-2/6">
        <RefreshIcon />
        <CreateCampaignButton />
      </div>
    </div>
  )
}
