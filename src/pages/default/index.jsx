import React from 'react'
import { WaveEmoji } from '../../components/emojis/WaveEmoji'
import { H1 } from '../../components/utils/H1'

export const DefaultPage = () => {
  return (
    <H1 classes="p-4">
      Welcome, Buddy! <WaveEmoji />
    </H1>
  )
}
