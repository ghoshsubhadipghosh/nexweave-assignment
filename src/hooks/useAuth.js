/**
 * @file
 * Contains a hook to retrieve authorized user details
 */

import DefaultUserAvatar from "../assets/user-avatar.png";

function useAuth() {
  /**
   * Get the profile details
   */
  const getProfile = () => {
    return {
      name: "Inder Verma",
      icon: DefaultUserAvatar,
      credit: 400,
    };
  };

  return {
    getProfile,
  };
}

export { useAuth };
