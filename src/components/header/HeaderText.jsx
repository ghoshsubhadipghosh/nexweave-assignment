import PropTypes from 'prop-types'
import { WaveEmoji } from "../emojis/WaveEmoji"

function HeaderText({ userName }) {
  return (
    <div className="text-primary-dark grid place-items-start text-base w-3/5">
      <p className="font-medium ml-4 text-center truncate">
        <span>How are you doing today </span>
        <span className="font-bold inline-block">{userName.split(" ")[0]}?</span>
        <span className="ml-2"><WaveEmoji /></span>
      </p>
    </div>
  )
}

HeaderText.propTypes = {
  userName: PropTypes.string.isRequired,
}

export { HeaderText }