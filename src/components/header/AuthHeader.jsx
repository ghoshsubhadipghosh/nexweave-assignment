import { useAuth } from "../../hooks/useAuth";
import { useCampaigns } from "../../hooks/useCampaigns";
import { UserAvatar } from "../avatar/UserAvatar";
import { DropDown } from "../dropdown/Dropdown";
import { HamburgerIcon } from "../logo/Hamburger";
import { HelpBoxIcon } from "../utils/HelpBoxIcon";
import { Credit } from "./Credit";
import { HeaderText } from "./HeaderText";


function AuthHeader() {
  const { getProfile } = useAuth();
  const { getCampaigns } = useCampaigns();

  const { icon, name, credit } = getProfile();
  const options = getCampaigns();

  return (
    <header className="text-primary-dark divide-x-2 p-2 h-18 flex flex-row flex-wrap items-center border-b-2 border-gray-300 border-opacity-80 w-full">
      {/* Divide 1 */}
      <div className="w-10/12 h-4/5 flex flex-row flex-wrap justify-around items-center">
        {/* First Section */}
        <div className="flex flex-row items-center justify-start w-8/12 pl-0 ml-0">
          {/* Hamburger Icon */}
          <HamburgerIcon />
          <HeaderText userName={name} />
        </div>
        {/* Second Section */}
        <div className="w-4/12 flex flex-row items-center">
          <DropDown options={options} />
          <Credit score={credit} />
        </div>
      </div>
      {/* Divide 2 */}
      <div className="w-2/12 h-4/5 flex flex-row items-center justify-around">
        <HelpBoxIcon />
        <UserAvatar src={icon} alt="user-avatar" />
      </div>
    </header>
  )
}

export { AuthHeader }