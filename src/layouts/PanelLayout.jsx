/**
 * @file
 * It's the default layout of any pages in the web app. where there's a default sidebar 
 */

import React from "react"
import { BrowserRouter, Route, Switch } from "react-router-dom"
import { AuthHeader } from "../components/header/AuthHeader"
import { Sidebar } from "../components/sidebar/Sidebar"
import { useAppRoutes } from "../hooks/useAppRoute"


function PanelLayout({ children }) {
  return (
    <div className="relative min-h-screen md:flex">
      {/* TODO: Mobile Navigation Link */}
      {/* Sidebar */}
      <Sidebar />
      {/* Actual Page */}
      <div className="flex-1 min-h-screen">
        {/* Header */}
        <AuthHeader />
        {/* Other Page Items */}
        <div className="page-content text-primary-dark">
          {children}
        </div>
      </div>
    </div>
  )
}

function AppRoutes() {
  const { getSidebarMenuItems } = useAppRoutes()
  const routes = getSidebarMenuItems()
  return (
    <BrowserRouter>
      <Switch>
        {routes.map(({ url, component: RouteComponent }) => (
          <Route
            key={url}
            path={url}
            exact
          >
            <PanelLayout>
              <RouteComponent />
            </PanelLayout>
          </Route>
        ))}
      </Switch>
    </BrowserRouter>
  )
}

export { PanelLayout, AppRoutes }