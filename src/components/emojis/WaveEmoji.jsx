
function WaveEmoji() {
  return (
    <span role="img" aria-label="wave">👋</span>
  )
}

export { WaveEmoji }