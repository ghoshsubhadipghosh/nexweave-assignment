import PropTypes from 'prop-types'

function Credit({ score }) {
  return (
    <div className="text-primary-dark h-full text-base pr-3 w-1/2">
      <span className="font-semibold opacity-60">Credits:</span>
      <span className="font-semibold ml-2">{score}</span>
    </div>
  )
}

Credit.propTypes = {
  /** Credit Score */
  score: PropTypes.number.isRequired,
}

export { Credit }