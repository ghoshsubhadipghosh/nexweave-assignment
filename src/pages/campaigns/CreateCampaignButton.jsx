import React from 'react'
import Button from '../../components/buttons/Button'
import { FiPlus } from 'react-icons/fi'

const CreateCampaignButton = props => {
  return (
    <div className="ml-2">
      <Button variant="outline">
        <FiPlus />
        Create a new campaign
      </Button>
    </div>
  )
}


export { CreateCampaignButton }
