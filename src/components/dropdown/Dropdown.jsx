import { Fragment, useCallback, useState } from 'react'
import { Listbox, Transition } from '@headlessui/react'
import { CheckIcon, SelectorIcon } from '@heroicons/react/solid'
import PropTypes from 'prop-types'

function DropDown({ options, onSelectCallback, defaultOptionIndex = 0 }) {

  if (options.length <= 0) {
    throw new Error(`Empty options are provided in DropDown component`)
  }

  const [selected, setSelected] = useState(options[defaultOptionIndex]);

  const onChangeCallback = useCallback((v) => {
    setSelected(v);
    // run callback function provided in the props
    if (onSelectCallback !== typeof Function) {
      console.error(`onSelectCallback must be function instead received ${typeof onSelectCallback}`)
    } else {
      onSelectCallback(v);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selected, onSelectCallback]);

  return (
    <div className="mr-8 w-full">
      <Listbox value={selected} onChange={onChangeCallback}>
        <div className="relative">
          <Listbox.Button className="relative w-full py-2 pl-3 pr-10 text-left bg-gray-100 bg-opacity-50 rounded-lg shadow-sm cursor-pointer focus:outline-none focus-visible:ring-2 focus-visible:ring-opacity-75 focus-visible:ring-white focus-visible:ring-offset-orange-300 focus-visible:ring-offset-2 focus-visible:border-indigo-500 sm:text-sm">
            <span className="block truncate font-semibold">{selected.name}</span>
            <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
              <SelectorIcon
                className="w-5 h-5 text-gray-400"
                aria-hidden="true"
              />
            </span>
          </Listbox.Button>
          <Transition
            as={Fragment}
            leave="transition ease-in duration-100"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Listbox.Options className="absolute w-full py-1 mt-1 overflow-auto text-base bg-white rounded-md shadow-lg max-h-60 ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
              {options.map((option, idx) => (
                <Listbox.Option
                  key={idx}
                  className={({ active }) =>
                    `${active ? 'text-amber-900 bg-amber-100' : 'text-gray-900'}
                          cursor-pointer select-none relative py-2 pl-10 pr-4 font-semibold`
                  }
                  value={option}
                >
                  {(optionCallback) => (
                    <>
                      <span
                        className={`${optionCallback.selected ? ' font-semibold' : ' font-semibold'
                          } block truncate`}
                      >
                        {option.name}
                      </span>
                      {optionCallback.selected ? (
                        <span
                          className={`${optionCallback.active ? 'text-amber-600' : 'text-amber-600'} absolute inset-y-0 left-0 flex items-center pl-3`}
                        >
                          <CheckIcon className="w-5 h-5" aria-hidden="true" />
                        </span>
                      ) : null}
                    </>
                  )}
                </Listbox.Option>
              ))}
            </Listbox.Options>
          </Transition>
        </div>
      </Listbox>
    </div>
  )
}

DropDown.propTypes = {
  options: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired
  })).isRequired,
  onSelect: PropTypes.func,
  defaultOptionIndex: PropTypes.number,
}


export { DropDown }