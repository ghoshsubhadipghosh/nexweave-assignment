import { BiQuestionMark } from "react-icons/bi";

function HelpBoxIcon({ onClick }) {

  const styles = {
    icon: {
      width: 26,
      height: 25,
      borderTopLeftRadius: '50%',
      borderTopRightRadius: '50%',
      borderBottomRightRadius: '50%',
      borderBottomLeftRadius: '1%',
    }
  }

  return (
    <div
      className="flex justify-items-center items-center cursor-pointer text-center border-2 border-primary px-1"
      onClick={onClick}
      style={styles.icon}
    >
      <BiQuestionMark />
    </div>
  )
}

export { HelpBoxIcon }