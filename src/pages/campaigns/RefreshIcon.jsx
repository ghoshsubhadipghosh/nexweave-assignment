import React from 'react'
import PropTypes from 'prop-types'
import { HiOutlineRefresh } from 'react-icons/hi'

const RefreshIcon = ({ onClick }) => {
  return (
    <div onClick={onClick} className="cursor-pointer w-10 h-10 bg-gray-200 bg-opacity-40 rounded-sm grid place-items-center shadow-sm">
      <HiOutlineRefresh className="text-2xl" />
    </div>
  )
}


RefreshIcon.propTypes = {
  onClick: PropTypes.func,
}

export { RefreshIcon }