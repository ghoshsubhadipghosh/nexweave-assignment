import PropTypes from 'prop-types'

function UserAvatar({ src, alt, onClick }) {
  return (
    <div className="rounded-full h-10 w-10 flex items-center justify-center" onClick={onClick}>
      <img src={src} alt={alt} className="rounded-full w-full h-full object-cover" />
    </div>
  )
}

UserAvatar.propTypes = {
  src: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
  onClick: PropTypes.func,
}

export { UserAvatar }