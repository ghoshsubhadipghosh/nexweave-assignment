/**
 * @file
 * 
 * Custom hamburger menu icon
 */

function HamburgerIcon({ onClick }) {
  return (
    <div className="hamburger-menu grid place-items-center h-16 w-16 relative cursor-pointer" onClick={onClick}>
      <div className="w-1/2 h-1/2">
        <span className="absolute block w-3/6 h-0.5 bg-primary mt-2 rounded-sm"></span>
        <span className="absolute block w-2/5 h-0.5 bg-primary mt-4 rounded-sm"></span>
        <span className="absolute block w-1/5 h-0.5 bg-primary mt-6 rounded-sm"></span>
      </div>
    </div>
  )
}

export { HamburgerIcon }