import logo from '../../assets/logo-dark.png'

function Logo() {
  return (
    <div className="flex flex-row justify-items-center items-center ta-center px-4 py-2">
      <div>
        <img src={logo} alt="Logo" className="w-10 h-10" />
      </div>
      <h1 className="px-3">
        <span className="nex text-secondary font-bold">Nex</span>
        <span className="weave text-primary font-bold">Weave.</span>
      </h1>
    </div>
  )
}

export { Logo }