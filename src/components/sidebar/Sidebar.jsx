/**
 * @file
 * Sidebar is contains all the links for the pages
 */

import React from 'react';
import { Logo } from '../logo/Logo';
import { useAppRoutes } from '../../hooks/useAppRoute';
import { Flex } from '../utils/Flex'
import { Link, useLocation } from 'react-router-dom';

function SidebarMenuItems() {
  const { getSidebarMenuItems } = useAppRoutes();
  const menus = getSidebarMenuItems()
  const { pathname: currentPage } = useLocation();


  const getContainerClasses = (url) => {
    return `cursor-pointer px-5 py-4 font-semibold text-primary-dark w-full ${url === currentPage && "border-l-4 border-secondary"}`;
  }

  const renderMenuItems = menus.map(({ name, url, Icon }) => (
    <Link to={url} key={name} className={getContainerClasses(url)}>
      <Flex>
        <div className="text-xl pl-1 pr-6 py-1 text-primary"><Icon /></div>
        <div className={`${url === currentPage ? "opacity-100" : "opacity-50"}`}>{name}</div>
      </Flex>
    </Link>
  ));

  return (
    <div className="flex flex-col m-0 p-0 w-full">
      {/* Logo */}
      <div className="py-4 px-2">
        <Logo />
      </div>
      {/* Routes / Navigation Links */}
      {renderMenuItems}
    </div>
  );
}


function Sidebar() {
  return (
    <nav>
      <div className="sidebar w-64 border-r-2 border-gray-300 border-opacity-80 min-h-screen space-y-6 absolute inset-y-0 left-0 transform -translate-x-full md:relative md:translate-x-0 transition duration-200 ease-in-out">
        <SidebarMenuItems />
      </div>
    </nav>
  )
}

export { Sidebar, SidebarMenuItems }