import "./App.css";
import { AppRoutes } from "./layouts/PanelLayout";

function App() {
  return <AppRoutes />;
}

export default App;
