import React from 'react'
import PropTypes from 'prop-types'

const Button = ({ variant, bgColor = 'bg-gray-200', color = 'primary', children, ...rest }) => {

  let classes = "";

  switch (variant) {
    case 'outline': {
      classes = "border-2 border-primary-dark bg-white"
      break;
    }
    case 'fill': {
      classes = `border-0 ${bgColor} text-${color}`;
      break;
    }
    default: {
      classes = "border-2 border-primary-dark bg-white"
    }
  }

  return (
    <button className={`font-semibold w-full h-10 text-center rounded-md flex justify-center items-center px-2 py-1 ${classes}`} {...rest}>
      {children}
    </button>
  )
}

Button.propTypes = {
  variant: PropTypes.oneOf([
    'outline',
    'fill'
  ]),
  bgColor: PropTypes.string,
  color: PropTypes.oneOf(['primary', 'secondary', 'primary-dark']),
  onClick: PropTypes.func
}

export default Button
