function Flex({ children }) {
  return (
    <div className="flex flex-row justify-items-center items-center">
      {children}
    </div>
  )
}

export { Flex };