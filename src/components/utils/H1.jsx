import React from 'react'

const H1 = ({ classes, children }) => {
  return (
    <h1 className={`text-2xl font-bold ${classes ? classes : ''}`}>
      {children}
    </h1>
  )
}


export { H1 }
