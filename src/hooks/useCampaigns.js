/**
 * This hooks is used to get the fetching campaign logic and other related logic
 */

function useCampaigns() {
  const campaigns = [
    { name: "Web Campaign" },
    { name: "Product Campaign" },
    { name: "Meet Campaign" },
  ];

  const getCampaigns = () => {
    return campaigns;
  };

  return {
    getCampaigns,
  };
}

export { useCampaigns };
